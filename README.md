======== Experimental MUA ==========
Currell Berry, 6/4/2018

This code is based on https://www.sqlite.org/cvstrac/wiki?p=ExperimentalMailUserAgent . This fork adds TLS support so you can use it with gmail/gmx/etc...  Also includes some compatibility patches/hacks which I found were necessary, at least on my platform openBSD.

