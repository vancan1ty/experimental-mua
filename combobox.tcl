#
# A combobox megawidget for Tk.
#
# This combobox implementation contains some features not normally found
# in a combobox.  In particular, it allows two different drop-down lists.
# The primary list is intended to be long and complete.  A alternative list
# with a different exclamation point icon is intended to have only one or
# two likely choices.  Either or both lists may be omitted, in which case
# their icons are not displayed.
#

package require Tk

# Create the ::combobox namespace to contain all the code and variables
# for the combobox implementation.
#
namespace eval ::combobox {
  variable v
}


# proc:   ::combobox::create WIDGET ARGS...
# title:  Create a new combobox widget
#
# Create a new combobox megawidget named WIDGET.  Configure the widget
# according to ARGS.  See the description of the ::combobox::config command
# for the details on ARGS.
#
proc ::combobox::create {w args} {
  upvar #0 ::combobox::v$w v
  frame $w -bd 2 -relief sunken
  bind $w <Destroy> [list ::combobox::_destroy $w]
  entry $w.e -bd 0 -fg black -bg white
  set top [winfo toplevel $w]
  bindtags $w.e [list $w.e Entry ComboboxEntry $top all]
  pack $w.e -side left -fill both -expand 1
  button $w.s -image ::combobox::down -takefocus 0 -highlightthick 0
  bindtags $w.s [list ComboboxButton $top all]
  button $w.a -image ::combobox::alt -takefocus 0 -highlightthick 0
  bindtags $w.a [list ComboboxAltButton $top all]
  toplevel $w.f -bd 0 -relief raised -cursor top_left_arrow -bg black
  bindtags $w.f [list ComboboxFrame all]
  wm overrideredirect $w.f 1
  wm withdraw $w.f
  catch {unset v}
  set v(height) 8
  set v(cvar) {}
  set v(choices) {}
  set v(altcvar) {}
  set v(altchoices) {}
  scrollbar $w.f.sb -orient vertical -command "$w.f.lb yview" -takefocus 0
  listbox $w.f.lb -yscrollcommand "$w.f.sb set" -bd 0 -exportselection 0 \
      -takefocus 1 -bg white -fg black -font [$w.e cget -font] \
      -highlightthickness 0 -width 1
  bindtags $w.f.lb [list ComboboxListbox all]
  pack $w.f.lb -side left -fill both -expand 1 -pady 1 -padx {1 0}
  pack $w.f.sb -side right -fill y -pady 1 -padx {0 1}
  eval ::combobox::config $w $args
}

# proc:   ::combobox::config WIDGET ARGS...
# title:  Configure a combobox
#
# The ARGS is a list of option/value pairs for the combobox WIDGET.
# Standard options (with the usual meaning) include:
#
# <ul>
# <li>   -takefocus
# <li>   -fg
# <li>   -foreground
# <li>   -bg
# <li>   -background
# <li>   -state
# </ul>
#
# In addition to the standard options shown above, the following custom
# options are available:
#
# <blockquote><dl>
# <dt> -width N        <dd>The width of the entry widget is N characters
#
# <dt> -height N       <dd>The maximum height of the drop-down listbox
#                      is N lines
#
# <dt> -variable V     <dd>The text shown in the entry widget is also
#                      written to variable V.  -textvariable is an alias
#                      for -variable.
#
# <dt> -choices L      <dd>L is a list of choices for the normal dropdown menu.
#                      The dropdown icon is only displayed if L is not empty.
#
# <dt> -cvar LV        <dd>LV is the name of a variable that contains
#                      the list of
#                      choices to display in the dropdown menu.  If both
#                      -choices and -cvar are specified, -cvar take precedence.
#
# <dt> -altchoices Q   <dd>Q is a list of choices for the alternative dropdown
#                      menu.  The alternative dropdown icon is only display
#                      if Q is not an empty list
#
# <dt> -altcvar QV     <dd>QV is the name of a variable that contains the list
#                      of options for the alternative dropdown menu.  If both
#                      -altchoices and -altcvar are specified, the -altcvar
#                      takes precedence.  (Changed on 2000-jan-18: The content
#                      of the -altcvar is assumed to be a single list element,
#                      even if it contains spaces.  But -cvar can contain a
#                      list.  -altchoices can also contain a list.)
#
# <dt> -editable BOOL  <dd>If BOOL is true, then the user can type directly into
#                      the combo box.  If false, then you must use the pulldown
#
# </dl></blockquote>
#
proc ::combobox::config {w args} {
  upvar #0 ::combobox::v$w v
  foreach {tag value} $args {
    switch -- $tag {
      -width {
        $w.e config -width $value
      }
      -state {
        $w.e config -state $value
        if {[$w.f.lb index end]>0} {
          $w.s config -state $value
        }
      }
      -takefocus {
        $w.e config -takefocus $value
      }
      -fg -
      -foreground {
        $w.e config -fg $value
        $w.f.lb config -bg $value
        $w.f config -bg $value
      }
      -bg -
      -background {
        $w.e config -bg $value
        $w.f.lb config -bg $value
      }
      -height {
        set v(height) $value
      }
      -choices {
        set v(choices) $value
        if {$value=="" && $v(cvar)==""} {
          pack forget $w.s
        } else {
          pack $w.s -after $w.e -side left -fill y
          $w.s config -state [$w.e cget -state]
        }
      }
      -cvar {
        if {$v(cvar)!=""} {
          trace vdelete $v(cvar) w [list ::combobox::_cvar_change $w]
        }
        set v(cvar) $value
        if {![info exists $value]} {set $value {}}
        if {$v(cvar)!=""} {
          trace variable $v(cvar) w [list ::combobox::_cvar_change $w]
        }
        if {$v(cvar)=="" && $v(choices)==""} {
          pack forget $w.s
        } else {
          pack $w.s -after $w.e -side left -fill y
          $w.s config -state [$w.e cget -state]
          _cvar_change $w
        }
      }
      -altchoices {
        set v(altchoices) $value
        if {$value=="" && $v(altcvar)==""} {
          pack forget $w.a
        } else {
          pack $w.a -side left -fill y
          $w.a config -state [$w.e cget -state]
        }
      }
      -altcvar {
        if {$v(altcvar)!=""} {
          trace vdelete $v(altcvar) w [list ::combobox::_cvar_change $w]
        }
        set v(altcvar) $value
        if {![info exists $value]} {set $value {}}
        if {$v(altcvar)!=""} {
          trace variable $v(altcvar) w [list ::combobox::_cvar_change $w]
        }
        if {$v(altcvar)=="" && $v(altchoices)==""} {
          pack forget $w.a
        } else {
          pack $w.a -side left -fill y
          $w.a config -state [$w.e cget -state]
          _cvar_change $w
        }
      }
      -textvariable -
      -variable {
        $w.e config -textvariable $value
      }
      -editable {
        if {$value} {
          bindtags $w.e [list $w.e Entry ComboboxEntry all]
          $w.e config -insertontime 600
        } else {
          bindtags $w.e [list ComboboxEntry all]
          $w.e config -insertontime 0
        }
      }
      -command {
        set v(command) $value
      }
      default {
        error "unknown combobox option: $tag"
      }
    }
  }
}

###############################################################################
# The public interface to this module is above this line.  Everything below
# this line is private to this module and should not be accessed or invoked
# from outside this module.
#

# The following image is used to give the down-arrow button on the right
# side of a combo box.
#
image create bitmap ::combobox::down -data \
"#define down_width 9\n#define down_height 8
static unsigned char down_bits[] = {
   0xff, 0x01, 0xfe, 0x00, 0xfe, 0x00, 0x7c, 0x00, 0x7c, 0x00, 0x38, 0x00,
   0x38, 0x00, 0x10, 0x00};"
image create bitmap ::combobox::alt -data \
"#define excl_width 8\n#define excl_height 8
static unsigned char excl_bits[] = {
   0x18, 0x3c, 0x3c, 0x18, 0x18, 0x00, 0x18, 0x18};"

# Default bindings for combobox components
#
bind ComboboxButton <1> {::combobox::_popup %W 0 1}
bind ComboboxAltButton <1> {::combobox::_popup %W 1 1}
bind ComboboxEntry <Up> {::combobox::_popup %W %s 0}
bind ComboboxEntry <Down> {::combobox::_popup %W %s 0}
bind ComboboxEntry <1> {if {[%W cget -state]!="disabled"} {focus %W}}
bind ComboboxFrame <ButtonRelease-1> {::combobox::_frame_release %W %y}
bind ComboboxFrame <B1-Motion> {::combobox::_frame_motion %W %y}
bind ComboboxListbox <Motion> {::combobox::_motion %W %y}
bind ComboboxListbox <ButtonRelease-1> {::combobox::_select %W %y}
bind ComboboxFrame <ButtonPress-1> {::combobox::_select %W.lb %y}
bind ComboboxFrame <Down> {::combobox::_updown %W 1}
bind ComboboxFrame <Up>   {::combobox::_updown %W -1}
bind ComboboxFrame <Return> {::combobox::_select %W.lb {}}
bind ComboboxFrame <Escape> {::combobox::_popdown %W}
bind ComboboxFrame <Tab> {::combobox::_updown %W 1; break}
bind ComboboxEntry <FocusIn> {::combobox::_focus_in %W}
bind ComboboxEntry <FocusOut> {::combobox::_focus_out %W}

# This command is executed when the users presses the down-arrow on the
# right-hand side of a combo box.  This routine causes the listbox dropdown
# to appear beneath the combobox and gives focus to the listbox.
#
# The $wx argument is the name of a subwidget that is one level above
# the root frame of the combobox. The $s argument is zero to use the menu
# options from -choices or non-zero to use -altchoices.  $d is 1 if
# $wx is button that should be depressed by this action and 0 if not.
#
proc ::combobox::_popup {wx s d} {
  regsub {\.[^.]+$} $wx {} w
  upvar #0 ::combobox::v$w v
  set s [expr {$s&0x5}]
  if {$s} {
    if {$v(altcvar)!=""} {
      set list [list [set $v(altcvar)]]
    } else {
      set list $v(altchoices)
    }
  } else {
    if {$v(cvar)!=""} {set list [set $v(cvar)]} {set list $v(choices)}
  }
  if {$list==""} {
    if {$s==0} return
    if {$v(cvar)!=""} {set list [set $v(cvar)]} {set list $v(choices)}
    if {$list==""} return
  }
  if {$d} {
    if {[$wx cget -state]=="disabled"} return
    $wx config -relief sunken
  }
  set x [winfo rootx $w]
  set y [winfo rooty $w]
  set width [winfo width $w]
  incr y [winfo height $w]
  $w.f.lb delete 0 end
  regsub -all \\s $list { } list
  eval $w.f.lb insert end $list
  set n [llength $list]
  if {$n<=$v(height)} {
    pack forget $w.f.sb
    pack $w.f.lb -padx 1
    $w.f.lb config -height $n
  } else {
    pack $w.f.sb -side right -fill y -padx {0 1}
    pack $w.f.lb -padx {1 0}
    $w.f.lb config -height $v(height)
  }
  set v(beenbelow) 0
  update idletasks
  wm geometry $w.f ${width}x[winfo reqheight $w.f]+$x+$y
  wm deiconify $w.f
  update
  $w.f.lb selection clear 0 end
  set pattern [$w.e get]
  if {$pattern=="" && $d} {set i -1} else {
    set i [lsearch -glob [$w.f.lb get 0 end] ${pattern}*]
  }
  if {$i>=0} {
    $w.f.lb selection set $i
    $w.f.lb activate $i
    $w.f.lb see $i
  }
  raise $w.f
  grab $w.f
  focus $w.f
  update
  after idle [subst {catch {raise $w.f}}]
  after 50 [subst {catch {raise $w.f}}]
}

# This routine makes the combobox listbox disappear.  $wx is the name
# name of some child of the root frame of the combobox.
#
proc ::combobox::_popdown wx {
  regsub {\.[^.]+$} $wx {} w
  grab release $w.f
  focus $w.e
  wm withdraw $w.f
}

# This routine runs when the user drags the mouse over the listbox
# widget.  Update the selection in the listbox to be that item that
# is closes to the mouse.  If the mouse goes below or above the listbox,
# start the motor to automatically scroll the listbox.
#
proc ::combobox::_motion {lb y} {
  set i [$lb index @1,$y]
  $lb selection clear 0 end
  $lb selection set $i
  regsub {\.[^.]+\.[^.]+$} $lb {} w
  upvar #0 ::combobox::v$w v
  set v(y) $y
  set v(dy) 0
  if {$y>5} {
    set v(beenbelow) 1
  }
  if {$y>[winfo height $lb]} {
    set v(dy) 1
  } elseif {$y<0 && $v(beenbelow)} {
    set v(dy) -1
  }
  if {$v(dy)!=0 && ![info exists v(motor)]} {
    set v(motor) [after 50 [list ::combobox::_motor $w]]
  }
}

# This routine runs as an idle callback.  Its job is to automatically
# scroll the listbox when the mouse moves above or below the listbox.
# $v(dy) indicates the direction that the listbox should scroll.
# -1 means scroll up and +1 means scroll down.  0 means do not scroll.
#
proc ::combobox::_motor {w} {
  upvar #0 ::combobox::v$w v
  catch {unset v(motor)}
  if {$v(dy)==0} return
  if {![winfo exists $w.f.lb] || ![winfo ismapped $w.f.lb]} return
  $w.f.lb yview scroll $v(dy) units
  _motion $w.f.lb $v(y)
}

# Put the currently selected listbox entry in the entry box.
#
proc ::combobox::_select {lb y} {
  upvar #0 ::combobox::v[winfo parent [winfo parent $lb]] v
  regsub {\.[^.]+\.[^.]+$} $lb {} w
  if {$y!=""} {
    if {$y<=0} {set i {}} else {
      set i [$lb index @1,$y]
    }
  } else {
    set i [lindex [$lb cursel] 0]
  }
  $w.s config -relief raised
  $w.a config -relief raised
  if {$i!=""} {
    set new [$w.f.lb get $i]
    if {$new!=[$w.e get]} {
      $w.e delete 0 end
      $w.e insert end $new
      if {[info exists v(command)] && $v(command)!=""} {
        uplevel #0 $v(command)
      }
    }
  }
  _popdown $w.e
}

# This routine is called when the mouse is being dragged outside the bounds
# of the listbox/scrollbar popdown while the left button is held down.
#
# If the mouse drops below the level of the top of the listbox, set the
# v(beenbelow) flag.  This will enable the upwards autoscroller if the
# mouse goes back above the top of the listbox again.
#
# Also, select whatever listbox element is closest to the mouse pointer.
#
proc ::combobox::_frame_motion {f y} {
  regsub {\.[^.]+$} $f {} w
  upvar #0 ::combobox::v$w v
  if {$y>5} {
    set v(beenbelow) 1
  }
  _motion $f.lb $y
}

# This routine is called when the mouse button is released.  If there has
# been significant mouse motion, then close the listbox popdown.  Otherwise,
# leave the listbox visible.
#
proc ::combobox::_frame_release {f y} {
  regsub {\.[^.]+$} $f {} w
  $w.s config -relief raised
  $w.a config -relief raised
  upvar #0 ::combobox::v$w v
  if {$v(beenbelow)} {
    _select $f.lb $y
  }
}

# This routine is called when the listbox is visible and the user presses
# either the up or down arrow key.  The result is to move the selected up
# up or down by one notch.  $dir is -1 to move up and +1 to move down.
#
proc ::combobox::_updown {f dir} {
  set lb $f.lb
  set i [lindex [$lb cursel] 0]
  if {$i==""} {
    $lb selection set 0
    $lb see 0
    return
  }
  set new [expr {int($i+$dir)}]
  set max [$lb index end]
  if {$new<0} {set new [expr {$max-1}]}
  if {$new>=$max} {set new 0}
  if {$new!=$i} {
    $lb selection clear 0 end
    $lb selection set $new
    $lb see $new
  }
}

# This routine is called when either the -cvar or -altcvar variables
# change.  The corresponding dropdown buttons are enabled or disabled
# as appropriate.
#
# 2002-feb-11 Bug #672: The complaint reads "The down arrow and the
# exclamation point display the same information!".  We'll fix this
# "problem" by turnning of the exclamation point if the altdata is
# the same as the main data.
#
proc ::combobox::_cvar_change {w args} {
  upvar #0 ::combobox::v$w v
  if {$v(cvar)!=""} {
    set v1 [set $v(cvar)]
    if {$v1!=""} {
      $w.s config -state normal
    } else {
      $w.s config -state disabled
    }
  } else {
    set v1 {}
  }
  if {$v(altcvar)!=""} {
    set v2 [set $v(altcvar)]
    if {$v2!="" && $v2!=$v1} {
      $w.a config -state normal
    } else {
      $w.a config -state disabled
    }
  }
}

# Bug #672, complaint 1a: When focus enters an uneditable combobox, we need
# to select the contents of that combobox so that the user can see that it
# has focus.  If the combobox is editable, then the Entry bindings will
# take care of this for us automatically.  But if it is not editable, there
# is no Entry binding, so we have to do it ourselves.
#
# Take care to remove the selection when focus leaves the widget.
#
proc ::combobox::_focus_in {w} {
  if {[lsearch [bindtags $w] Entry]<0} {
    $w select range 0 end
    # $w config -bg [$w cget -selectbackground] -fg [$w cget -selectforeground]
  }
}
proc ::combobox::_focus_out {w} {
  if {[lsearch [bindtags $w] Entry]<0} {
    $w select clear
    # $w config -bg white -fg black
  }
}

# This routine is called when a combobox is destroyed.  This routine
# cleans up the status variable and any traces that the combobox may
# have created.
#
proc ::combobox::_destroy {w} {
  upvar #0 ::combobox::v$w v
  if {$v(cvar)!=""} {
    trace vdelete $v(cvar) w [list ::combobox::_cvar_change $w]
  }
  if {$v(altcvar)!=""} {
    trace vdelete $v(altcvar) w [list ::combobox::_cvar_change $w]
  }
  unset v
}

# The following code implements a simple unit test of this module.  The
# code will only run if the ::main::focus procedure is not defined, which
# means it will only run if this TCL script is sourced in isolation, not
# as part of the whole TPE.
#
if {0} {
  ::combobox::create .f1 -width 20 -choices {One Two Three Four Five} \
      -variable xyz -editable 0
  pack .f1 -side top -padx 20 -pady 20
  set ::alllist {}
  for {set i 0} {$i<40} {incr i} {lappend alllist $i}
  ::combobox::create .f2 -width 30 -choices $alllist \
      -height 8 -variable abc -altchoices {2 3 5}
  pack .f2 -side top -padx 20 -pady 20
  ::combobox::create .f3 -width 2 -cvar ::longlist -altcvar ::shortlist \
      -variable pqr
  pack .f3 -side top -padx 20 -pady 20
  trace variable abc w change_list
  proc change_list {args} {
    set ::longlist [lrange $::alllist 1 $::abc]
    set ::shortlist [lrange $::alllist 1 [expr {$::abc/3}]]
  }
  set ::abc 40
  change_list
  label .l1 -textvariable xyz -bd 1 -relief sunken -width 10
  label .l2 -textvariable abc -bd 1 -relief sunken -width 10
  label .l3 -textvariable pqr -bd 1 -relief sunken -width 10
  pack .l1 .l2 .l3 -side left -pady 30 -padx 4 -expand 1
}
