################################# compose.tcl ################################
# This module implements an email composer and sender widget
#

# Bring up a window that is used to compose a new email message.
# The window is initially blank.  Muliple composer windows can
# exist at the same time.
#
proc compose_begin {} {
  set i 1
  while {[winfo exists .n$i]} {incr i}
  set w .n$i
  upvar #0 v$w v
  unset -nocomplain v
  set v(attach) {}
  toplevel $w
  wm title $w {Compose Email}
  wm iconname $w [wm title $w]
  frame $w.b
  pack $w.b -side bottom -fill x
  button $w.b.send -text Send -command "compose_send $w"
  button $w.b.save -text Save -command "compose_save $w 7"
  button $w.b.can -text Cancel -command "compose_cancel $w"
  pack $w.b.send $w.b.save $w.b.can -side right -expand 1 -padx 5 -pady 5

  notebook::create $w.n -pages {Header Body Attachments} \
      -padx 5 -pady 5
  pack $w.n -side top -fill both -expand 1

  frame $w.n.p0.top
  pack $w.n.p0.top -side top -fill both -expand 1
  text $w.n.p0.top.t -yscrollcommand "$w.n.p0.top.vsb set" -bg white
  pack $w.n.p0.top.t -side left -fill both -expand 1
  scrollbar $w.n.p0.top.vsb -orient vertical -command "$w.n.p0.top.t yview"
  pack $w.n.p0.top.vsb -side left -fill y
  frame $w.n.p0.btm
  pack $w.n.p0.btm -side bottom -fill x
  text $w.n.p0.btm.t -yscrollcommand "$w.n.p0.btm.vsb set" -height 8 \
      -cursor top_left_arrow
  bindtags $w.n.p0.btm.t [list $w.n.p0.btm.t $w all]
  $w.n.p0.btm.t tag config to -foreground blue
  $w.n.p0.btm.t tag bind to <1> "compose_add_user $w to %W %y"
  $w.n.p0.btm.t tag config cc -foreground blue
  $w.n.p0.btm.t tag bind cc <1> "compose_add_user $w cc %W %y"
  pack $w.n.p0.btm.t -side left -fill both -expand 1
  scrollbar $w.n.p0.btm.vsb -orient vertical -command "$w.n.p0.btm.t yview"
  pack $w.n.p0.btm.vsb -side left -fill y
  label $w.n.p0.lbl -text Search:
  pack $w.n.p0.lbl -side left -pady {5 0}
  entry $w.n.p0.e -bd 2 -relief sunken -bg white -fg black -width 30
  pack $w.n.p0.e -side left -pady {5 0}
  bind $w.n.p0.e <Return> "$w.n.p0.go invoke"
  button $w.n.p0.go -text Go -command "compose_user_search $w"
  button $w.n.p0.addheaders -text {Add Headers} -padx 0 -pady 0 \
    -command "message_add_headers $w"

  pack $w.n.p0.go -side left -pady {5 0}
  pack $w.n.p0.addheaders -side left -pady {5 0}

  compose_create_message_id $w

  frame $w.n.p1.b
  pack $w.n.p1.b -side bottom -fill x
  button $w.n.p1.b.sig -text Signature -command "compose_sign $w"
  button $w.n.p1.b.cut -text Cut -command "cut_text $w.n.p1.t"
  button $w.n.p1.b.copy -text Copy -command "copy_text $w.n.p1.t"
  button $w.n.p1.b.paste -text Paste -command "paste_text $w.n.p1.t"
  pack $w.n.p1.b.sig $w.n.p1.b.cut $w.n.p1.b.copy $w.n.p1.b.paste \
     -side left -padx 5 -expand 1
  frame $w.n.p1.subj
  pack $w.n.p1.subj -side top -fill x
  label $w.n.p1.subj.l -text Subject:
  pack $w.n.p1.subj.l -side left
  entry $w.n.p1.subj.e -bd 2 -relief sunken -bg white -fg black
  pack $w.n.p1.subj.e -side left -fill x -expand 1
  text $w.n.p1.t -yscrollcommand "$w.n.p1.vsb set" -wrap word -bg white
  pack $w.n.p1.t -side left -fill both -expand 1
  scrollbar $w.n.p1.vsb -orient vertical -command "$w.n.p1.t yview"
  pack $w.n.p1.vsb -side left -fill y

  text $w.n.p2.t -yscrollcommand "$w.n.p2.vsb set" -wrap word \
    -cursor top_left_arrow
  bindtags $w.n.p2.t [list $w.n.p2.t $w all]
  $w.n.p2.t tag config add -foreground blue
  $w.n.p2.t tag bind add <1> "compose_add_attachment $w"
  $w.n.p2.t tag config del -foreground blue
  $w.n.p2.t tag bind del <1> "compose_delete_attachment $w %y"
  pack $w.n.p2.t -side left -fill both -expand 1
  scrollbar $w.n.p2.vsb -orient vertical -command "$w.n.p2.t yview"
  pack $w.n.p2.vsb -side left -fill y
  compose_paint_attachment_screen $w

  bind $w.n <Configure> [subst {
    ::notebook::config $w.n -width %w -height %h
  }]
    
  return $w
}

# Cancel a compose session
#
proc compose_cancel {w} {
  upvar #0 v$w v
  unset -nocomplain v
  destroy $w
}

# Search for users.
#
# This routine looks at the search string in the email search entry box
# in the middle of the first page.  Any email address in the database
# that match the search string are put in the lower box of that same page.
# Hyperlinks on each email address will copy the addresses into the
# upper window.
#
proc compose_user_search {w} {
  set pattern %[$w.n.p0.e get]%
  set t $w.n.p0.btm.t
  $t delete 1.0 end
  db eval {SELECT fulladdr FROM user WHERE fulladdr LIKE $pattern} {
    $t insert end $fulladdr addr { [to]} to " \[cc\]\n" cc
  }
}

# Add a user to the To:/Cc: set for an email message.
#
# This routine is called when the user clicks on one of the "[cc]"
# or "[to]" hyperlinks in the search results window at the bottom of
# the first page.  Copy the email address on the line that was clicked
# into the upper window.
#
proc compose_add_user {w type t y} {
  set loc [$t index @1,$y]
  set line [expr {int($loc)}]
  set first $line.0
  set last [$t index [list $first lineend]]
  foreach {first last} [$t tag nextrange addr $first $last] break
  set fulladdr [$t get $first $last]
  set t2 $w.n.p0.top.t
  if {$type=="to"} {
    $t2 insert end "To: $fulladdr\n"
  } else {
    $t2 insert end "Cc: $fulladdr\n"
  }
}

# Add a new attachment to an email message
#
proc compose_add_attachment {w} {
  set types {{{Allfiles} *}}
  set fn [tk_getOpenFile -filetypes $types -parent $w]
  if {$fn==""} return
  upvar #0 v$w v
  lappend v(attach) [list file [file tail $fn] $fn]
  compose_paint_attachment_screen $w
}

# Delete an attachment. 
#
# This routine is called when the user clicks on the [del]
# hyperlink of the attachment page.  $y is the y coordinate of
# the click and is use to figure out which attachment to delete.
#
proc compose_delete_attachment {w y} {
  set line [expr {int([$w.n.p2.t index @1,$y])}]
  upvar #0 v$w v
  incr line -1
  set v(attach) [lreplace $v(attach) $line $line]
  compose_paint_attachment_screen $w
}

# Redraw the attachment screen
#
# Reconstruct all of the text in the attachment page using
# information in the v(attach) variable.
#
proc compose_paint_attachment_screen {w} {
  set t $w.n.p2.t
  $t delete 1.0 end
  upvar #0 v$w v
  set i 0
  foreach record $v(attach) {
    incr i
    foreach {type name src} $record break
    if {$type=="file"} {
      set size [file size $src]
    } else {
      set size [string length $src]
    }
    if {$size>1024*1024} {
      set size [format {%.1f MiB} [expr {$size/(1024.0*1024.0)}]]
    } elseif {$size>1024} {
      set size [format {%.1f KiB} [expr {$size/(1024.0)}]]
    } else {
      set size "$size bytes"
    }
    $t insert end "($i) $name ($size)" {} " \[delete\]\n" del
  }
  $t insert end "\[add\]" add
}

# Extract the current email message as a mime token and a list of
# header values.
#
# The calling function is expected to release the token by calling
#
#     mime::finalize $token -subordinates all
#
# If an unrecoverable error is encountered, an empty string is
# returned instead of a mime token and header values list.
#
# The To:, Cc:, Subject:, and From: headers are only added if the
# $addhdr flag is true.  The smtp module needs to keep these separate.
#
proc compose_build_mime_token {w addhdr} {
  # The $hdr variable will contain one or more -header options to
  # be passed to the top-level mime in order to set the email header
  #
  set hdr [list -header [list From \
              [db eval {SELECT value FROM var 
                        WHERE name1='config' AND name2='fromaddr'}]]]

  # Process the addresses in the address window.  Process them from
  # top to bottom.  If a duplicate email is seen, only send to the
  # version that appears closes to the bottom of the input window.
  #
  set i 0
  foreach hdrline [split [$w.n.p0.top.t get 1.0 end] \n] {
    incr i
    set hdrline [string trim $hdrline]
    if {$hdrline==""} continue
    set type to
    if {[regexp -nocase {^to: (.*)} $hdrline all x]} {
      set hdrline $x
    } elseif {[regexp -nocase {^cc: (.*)} $hdrline all x]} {
      set hdrline $x
      set type cc
    } elseif {[regexp {^([a-zA-Z0-9-]+): (.*)$} $hdrline all field value]} {
      lappend hdr -header [list $field $value]
      continue
    } else {
      notebook::raise $w.n 0
      set r [tk_messageBox -icon error -type okcancel -parent $w -message \
                "Bad header: $hdrline"]
      if {$r=="ok"} continue
      return {}
    }
    unset -nocomplain x
    array set x [lindex [mime::parseaddress $hdrline] 0]
    if {[info exists x(error)] && $x(error)!=""} {
      notebook::raise $w.n 0
      set r [tk_messageBox -icon error -type okcancel -parent $w -message \
                $x(error)]
      if {$r=="ok"} continue
      return {}
    } else {
      set a${type}($x(address)) [list [format %05d $i] $x(proper)]
    }
    unset x
  }
  set list1 {}
  foreach x [array names ato] {
    lappend list1 $ato($x)
  }
  set list2 {}
  foreach x [lsort $list1] {
    lappend list2 [lindex $x 1]
  }
  lappend hdr -header [list To [join $list2 {, }]]
  set list1 {}
  foreach x [array names acc] {
    lappend list1 $acc($x)
  }
  set list2 {}
  foreach x [lsort $list1] {
    lappend list2 [lindex $x 1]
  }
  lappend hdr -header [list Cc [join $list2 {, }]]

  # Add the subject line to the header.
  #
  lappend hdr -header [list Subject [$w.n.p1.subj.e get]]

  # Get the body of the email message
  #
  set body [$w.n.p1.t get 1.0 end]

  # Build the main mime.  If there are no attachments, then the
  # top-level mime is text/plain and contains the body of the message.
  # If there are attachments, then the toplevel mime is multipart/mixed.
  # The body is in the first child mime and is of type text/plain.  All
  # attachments are in subsequent child mimes of type application/octet-stream.
  #
  upvar #0 v$w v
  if {[llength $v(attach)]==0} {
    set cmd [list mime::initialize -canonical text/plain -string $body]
  } else {
    set parts [mime::initialize -canonical text/plain -string $body]
    foreach record $v(attach) {
      foreach {type name src} $record break
      if {$type=="file"} {
         lappend parts [mime::initialize -canonical application/octet-stream \
                            -file $src -encoding base64 \
                            -param [list name $name]]
      } elseif {$type=="fwd"} {
         lappend parts [mime::initialize -canonical message/rfc822 \
                            -string $src -encoding 7bit]
      } else {
         lappend parts [mime::initialize -canonical application/octet-stream \
                            -string $src -encoding base64 \
                            -param [list name $name]]
      }
    }
    set cmd [list mime::initialize -canonical multipart/mixed -parts $parts]
  }
  if {$addhdr} {
    set cmd [concat $cmd $hdr]
  }
  set token [eval $cmd]
  # puts [mime::buildmessage $token]
  return [list $token $hdr]
}

# This routine is called when the user presses the Send button on the
# email composer.
#
proc compose_send {w} {
  set r [compose_build_mime_token $w 0]
  if {$r==""} return
  set m [lindex $r 0]
  set hdr [lindex $r 1]
  wm withdraw $w
  raise .t
  raise .vsb
  .t delete 1.0 end
  if {[catch {compose_send_mime $m $hdr} msg]} {
    mime::finalize $m -subordinates all  
    compose_puts \n**********************************\n
    compose_puts $msg\n\n
    compose_puts $::errorInfo
    compose_save $w 7
    message_rebuild_catalog
    raise .t
    raise .vsb
  } else {
    mime::finalize $m -subordinates all  
    compose_save $w 6
  }
  compose_cancel $w
}

# Write a log message about a send in progress.
#
proc compose_puts {msg} {
  .t insert end $msg
  update
}

# This routine does the work of sending an email message.  This code
# is encapsulated in a separate procedures so that errors can be easily
# caught.
#
proc compose_send_mime {m hdr} {
  array set x [db eval {SELECT name2, value FROM var
                        WHERE name1='config' AND name2 LIKE 'smtp%'}]
  compose_puts "Attempting to send to $x(smtphost):$x(smtpport)... "

  set opts {} 
  lappend opts -servers [list $x(smtphost)] 
  lappend opts -ports [list $x(smtpport)] 
  lappend opts -username [list $x(smtpuserid)] 
  lappend opts -password [list $x(smtppasswd)]


  compose_puts "\n sending \n"
  smtp::sendmessage $m {*}$opts {*}$hdr -debug 1 -usetls 1 -queue false -atleastone false 
  compose_puts OK
}

# Add the signature line to the bottom of the message text.
#
proc compose_sign {w} {
  set sig [db one {SELECT value FROM var
                   WHERE name1='config' AND name2='signature'}]
  $w.n.p1.t insert end $sig
}

# This routine is called when the user presses the Save button on the
# email composer.  Instead of sending the email, it is just added to
# the database.
#
proc compose_save {w priority} {
  set r [compose_build_mime_token $w 1]
  if {$r==""} return
  set m [lindex $r 0]
  compose_cancel $w
  message_insert [mime::buildmessage $m] $priority
  mime::finalize $m -subordinates all  
  message_rebuild_catalog
}

# Add records to the header of the email currently being composed.
#
proc compose_add_header {w label value} {
  set t $w.n.p0.top.t
  $t insert end "$label: $value\n"
}

# The $label argument is either To: or Cc:.  For each element of the
# $addrlist parameter, add a single entry in the header.
#
proc compose_add_addresses {w label addrlist} {
  upvar #0 v$w v
  foreach addr $addrlist {
    foreach paddr [mime::parseaddress $addr] {
      unset -nocomplain x
      array set x $paddr
      if {[info exists x(address)] && ![info exists v(exclude:$x(address))]} {
        set v(exclude:$x(address)) 1
        compose_add_header $w $label $x(proper)
      }
    }
  }
}

# Initialize the email exclusion list for composer window $w.
# The email exclusion list contains email addresses that will not
# be automatically added to the header for a reply.  Addresses
# excluded typically include the replier's address.
#
# The $rcvd parameter (if it is not an empty string) is the content
# of the Received: parameters in the header of an email.
#
proc compose_init_exclude {w rcvd} {
  upvar #0 v$w v
  if {[regexp {for <([a-zA-Z0-9.@-]+)>} $rcvd all x]} {
    set v(exclude:$x) 1
  } elseif {[regexp {<([a-zA-Z0-9.@-]+)>} $rcvd all x]} {
    set v(exclude:$x) 1
  }
  set email [db one {SELECT value FROM var WHERE name1='config'
                     AND name2='fromaddr'}]
  if {$email!=""} {
    set v(exclude:$email) 1
  }
}

# Add a message-id to the header section
#
proc compose_create_message_id {w} {
  set sender [db one {SELECT value FROM var WHERE name1='config'
                     AND name2='fromaddr'}]
  if {$sender!=""} {
    regsub {^.*@} $sender {} sender
    append msgid [db one {SELECT julianday('now') || abs(random())
                                 || abs(random())}]
    append msgid @$sender
    compose_add_header $w Message-ID <$msgid>
  }
}

# CB for gmx interop I found that the Date header needed to be appended, this method
# facilitates adding an appropriate formatted Date header as well as a To header.
proc message_add_headers {w} {
    compose_add_header $w Date \
                       [clock format [clock seconds] -format "%a, %e %b %Y %H:%M:%S %z %Z"]
    compose_add_header $w To {<testaddress@example.com>}
}

# This routine will automatically fill in the To: and Subject:
# etc in response to a Reply button press.  If $allflag==1 then
# process for Reply-All instead of just plain Reply.
#
proc compose_reply {w allflag} {
  global selected
  db eval {
    SELECT data, datetime(rcvd) AS 'when'
      FROM msg JOIN blob USING(blobid)
     WHERE msgid=$selected
  } break
  if {![info exists data]} return
  set m [mime::initialize -string $data]
  set hdrlist [mime::getheader $m]
  set body [message_body $m]
  mime::finalize $m
  set hdr(received) {}
  foreach {name value} $hdrlist {
    set hdr([string tolower $name]) $value
  }
  compose_init_exclude $w $hdr(received)
  if {[info exists hdr(subject)]} {
    set subj [subject_fixup [lindex $hdr(subject) 0]]
    $w.n.p1.subj.e delete 0 end
    $w.n.p1.subj.e insert 0 "Re: $subj"
  }
  if {[info exists hdr(message-id)]} {
    compose_add_header $w In-Reply-To [lindex $hdr(message-id) 0]
    compose_add_header $w References [lindex $hdr(message-id) 0]
  }
  if {[info exists hdr(reply-to)]} {
    compose_add_addresses $w To $hdr(reply-to)
  } elseif {[info exists hdr(from)]} {
    compose_add_addresses $w To $hdr(from)
  }
  if {$allflag} {
    if {[info exists hdr(to)]} {
      compose_add_addresses $w To $hdr(to)
    }
    if {[info exists hdr(cc)]} {
      compose_add_addresses $w Cc $hdr(cc)
    }
  }
  if {[info exists hdr(from)]} {
    set from [lindex $hdr(from) 0]
  } else {
    set from *anonymous*
  }
  regsub -all \n \n[string map {\r {}} [string trim $body]] "\n> " body
  $w.n.p1.t insert end "$from wrote:\n"
  $w.n.p1.t insert end [string trim $body]\n
}

# This routine sets up a newly created edit buffer to forward
# the currently selected email.
#
proc compose_forward {w} {
  global selected
  db eval {
    SELECT data, subject
      FROM msg JOIN blob USING(blobid)
     WHERE msgid=$selected
  } break
  if {![info exists data]} return
  regsub -all {Re: *} $subject {} subj
  $w.n.p1.subj.e delete 0 end
  $w.n.p1.subj.e insert 0 "Fwd: $subj"
  upvar #0 v$w v
  lappend v(attach) [list fwd {*forwarded email msg*} $data]
  compose_paint_attachment_screen $w
}
