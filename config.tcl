############################## config.tcl ################################
# This module contains code used to bring up a configuration screen
# for the email client.
#

# Popup the main configuration screen.
#
proc config_popup {} {
  set w .config
  if {[winfo exists $w]} {
    wm deiconify $w
    raise $w
    config_load
    return
  }
  toplevel $w
  wm title $w {Email Configuration}
  wm iconname $w [wm title $w]
  frame $w.b
  pack $w.b -side bottom -fill x
  button $w.b.ok -text Ok -width 6 -command {config_save; destroy .config}
  button $w.b.can -text Cancel -width 6 -command {destroy .config}
  pack $w.b.ok $w.b.can -side left -expand 1 -pady 5
  frame $w.f
  pack $w.f -side top -fill both -expand 1 -padx 10 -pady 10
  set i 0
  global config_textwidget
  foreach {name var width height} {
    {POP3 Server Hostname}     pop3host 30 1
    {POP3 Server TCP Port}     pop3port 6 1
    {POP3 User Id}             pop3userid 30 1
    {POP3 Password}            pop3passwd 30 1
    {SMTP Server Hostname}     smtphost 30 1
    {SMTP Server TCP Port}     smtpport 6 1
    {SMTP UserID (usually same as POP3's)} smtpuserid 30 1
    {SMTP Password (usually same as POP3's)} smtppasswd 30 1
    {Human-Friendly User Name} friendlyname 30 1        
    {Email Address}            fromaddr 30 1
    {Signature}                signature 50 4

  } {
    incr i
    set m $w.f.m$i
    label $m -text $name: 
    grid $m -row $i -column 1 -sticky ne
    set m $w.f.e$i
    if {$height<=1} {
      entry $m -bd 2 -relief sunken -bg white -fg black \
          -textvariable ::config($var) \
          -width $width
    } else {
      text $m -width $width -height $height -bg white -fg black
      set config_textwidget($var) $m
    }
    grid $m -row $i -column 2 -sticky w
  }
  config_load
}

# Move all of the configuration values out of the database and into
# the global ::config array
#
proc config_load {} {
  global config config_textwidget
  array set config \
    [db eval {SELECT name2, value FROM var WHERE name1='config'}]
  foreach v [array names config_textwidget] {
    $config_textwidget($v) delete 1.0 end
    if {[info exists config($v)]} {
      $config_textwidget($v) insert end $config($v)
    }
  }
}

# Move the configuration parameters in global ::config back into
# the database
#
proc config_save {} {
  global config config_textwidget
  foreach v [array names config_textwidget] {
    set config($v) [string trim [$config_textwidget($v) get 1.0 end]]
  }
  db transaction {
    foreach name2 [array names config] {
      set val $config($name2)
      db eval {REPLACE INTO var(name1,name2,value) VALUES('config',$name2,$val)}
    }
  }
}
