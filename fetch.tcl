################################ fetch.tcl ##################################
# This module contains code used to extract email from a POP3
# server and add it to the email database
#

package require tls 1.6

# Begin fetching email
#
proc fetch_begin {} {
  if {[.bb.fetch cget -text]=="Cancel"} {
    fetch_end
    return
  }
  .bb.fetch config -text Cancel
  show_msg
  .t delete 1.0 end
  if {[catch {fetch_run} msg]} {
    bell
    fetch_message \n***********************************\n
    fetch_message $msg\n
    fetch_message $::errorInfo
  }
  fetch_end
}


# This routine starts the mail fetch process.  The global variable fetch()
# holds the process state.  This routine does not return until the mail
# fetch is complete, though update is called frequently.
#
proc fetch_run {} {
  global fetch
  unset -nocomplain fetch
  set fetch(pop3port) 110
  array set fetch [db eval {
     SELECT name2, value FROM var WHERE name1='config' AND name2 LIKE 'pop3%'
  }]
  fetch_message "Opening connection to $fetch(pop3host)...\n"
  if {[catch {
    set fetch(socket) [::tls::socket $fetch(pop3host) $fetch(pop3port)]
  } errmsg]} {
    fetch_message "Error: $errmsg\n"
    return
  }
  fconfigure $fetch(socket) -translation auto -blocking 0
  fileevent $fetch(socket) readable fetch_readable
  fetch_reset_timer
  set fetch(done) 0
  set fetch(mode) init
  after 1000 [subst -nocommand {flush $fetch(socket); puts "p2s:[gets $fetch(socket)]:p2e"}]
  vwait ::fetch(done)
  catch {close $fetch(socket)}
  catch {after cancel $fetch(timer)}
  unset -nocomplain fetch
}

# Reset the timeout timer.
#
proc fetch_reset_timer {} {
  global fetch
  if {[info exists fetch(timer)]} {
    after cancel $fetch(timer)
  }
  set fetch(timer) [after 30000 {fetch_cancel {Timeout}}]
}

# Cancel the mail fetch process
#
proc fetch_cancel {errmsg} {
  global fetch
  after cancel $fetch(timer)
  fileevent $fetch(socket) readable {}
  close $fetch(socket)
  fetch_message $errmsg
  set fetch(done) 1
}

# Send a single message to the POP3 server
#
proc fetch_send {line {alt {}}} {
  puts "-$line"
  global fetch
  if {$alt==""} {
    fetch_message $line\n
  } else {
    fetch_message $alt\n
  }
  puts $fetch(socket) $line
  flush $fetch(socket)
}

# A fileevent invokes this routine whenever there is text that can be
# read from the POP3 server.  One line of input is processed.
#
proc fetch_readable {} {
  global fetch
  flush $fetch(socket)
  if {$fetch(done)} return
  fetch_reset_timer
  set line [gets $fetch(socket)]
  puts "line: \[$line\]"
  # if {$line==""} {puts "looping"; return}
  if {$line=="" && [fblocked $fetch(socket)]} return
  if {$fetch(mode)=="get"} {
    append fetch(msg) $line\n
    if {$line=="." || $line==".\r"} {
      set body $fetch(msg)
      set fetch(msg) {}
      if {$body!=""} {
        if {[catch {message_insert $body 5} msg]} {
          set errfile [glob ~]/bad_email_
          append errfile [clock format [clock seconds] -format {%Y%m%d_%H%M%S}]
          fetch_message "*** error: $msg\n"
          fetch_message "*** mail content saved in $errfile\n"
          set out [open $errfile w]
          fconfigure $out -translation auto
          puts -nonewline $out $body
          close $out
          after 1000
        }
      }
      incr fetch(i)
      if {$fetch(i)<=$fetch(count)} {
         set fetch(mode) retr
         fetch_send "RETR $fetch(i)"
      } else {
         set fetch(i) 1
         set fetch(mode) dele
         fetch_send "DELE 1"
#         set fetch(mode) quit
#         fetch_send "QUIT"
      }
    }
   return
  }
  fetch_message [string trim $line]\n
  puts "line: '$line'"
  if {[string range $line 0 2]!="+OK"} {
    puts p1
    fetch_cancel {Protocol error}
    return
  }
  if {$fetch(mode)=="retr"} {
    set fetch(mode) get
    set fetch(msg) {}
  } elseif {$fetch(mode)=="stat"} {
    if {[scan $line {+OK %d %d} count size]==2} {
      if {$count==0} {
        fetch_cancel Done
      } else {
        set fetch(i) 1
        set fetch(count) $count
        set fetch(mode) retr
        fetch_send "RETR 1"
      }
    } else {
      puts p2
      fetch_cancel "Protocol error"
    }
  } elseif {$fetch(mode)=="dele"} {
    incr fetch(i)
    if {$fetch(i)<=$fetch(count)} {
      fetch_send "DELE $fetch(i)"
    } else {
      set fetch(mode) quit
      fetch_send "QUIT"
    }
  } elseif {$fetch(mode)=="quit"} {
    fetch_cancel {Done}
  } elseif {$fetch(mode)=="user"} {
    fetch_send "PASS $fetch(pop3passwd)" {PASS ************}
    set fetch(mode) pass
  } elseif {$fetch(mode)=="pass"} {
    fetch_send "STAT"
    set fetch(mode) stat
  } elseif {$fetch(mode)=="init"} {
    fetch_send "USER $fetch(pop3userid)"
    set fetch(mode) user
  }
}

# Show the given message in the fetch window
#
proc fetch_message {msg} {
  .t insert end $msg
  .t yview end
}

# Terminate a fetch operation
#
proc fetch_end {} {
  global fetch
  if {[info exists fetch(done)] && !$fetch(done)} {
    fetch_cancel {Stopped by user request}
  }
  unset -nocomplain fetch
  .bb.fetch config -text Fetch
  message_rebuild_catalog
  show_msg
}
