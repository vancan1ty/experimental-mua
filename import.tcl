################################ import.tcl #################################
# This module contains code used to import email message into the
# email database from other sources.
#

# Import email messages from a text file.  Each email messaeg is assumed
# to begin with a line that starts with "From ".
#
proc import_from_file {} {
  set types {{{All Files} *}}
  set file [tk_getOpenFile -filetypes $types]
  if {$file==""} return
  set in [open $file]
  fconfigure $in -translation auto
  set buffer {}
  show_msg
  .t delete 1.0 end
  db transaction {
    while {![eof $in]} {
      set line [gets $in]
      if {[regexp {^From } $line]} {
        if {[string length $buffer]>0} {
          message_insert $buffer 3
          .t insert end $fromline\n
          .t see end
          update
        }
        set buffer "X-From: [string range $line 5 end]\n"
        set fromline $line
      } else {
        append buffer $line\n
      }
    }
    close $in
    if {[string length $buffer]>0} {
      message_insert $buffer 3
    }
  }
  message_rebuild_catalog_when_idle
}


# Import email messages from another email database.  Only import those
# messages whose msg_uuid (corresponding to Message-ID: in the header)
# is not already found in our local msg_uuid header.  By only importing
# new messages this way, we can "import" from other database in order
# to bring two or more database into alignment.
#
proc import_from_db {} {
  set types {{{All Files} *}}
  set file [tk_getOpenFile -filetypes $types]
  if {$file==""} return
  db eval "attach '[string map {' ''} $file]' as aux"
  set rc [catch _import_from_aux msg]
  db eval "detach aux"
  if {$rc} {error $msg}
}
proc _import_from_aux {} {
  show_msg
  .t delete 1.0 end
  db transaction {
    db eval {
       SELECT priority, data
         FROM aux.msg, aux.blob, aux.msg_uuid
        WHERE aux.blob.blobid=aux.msg.blobid
          AND aux.msg_uuid.msgid=aux.msg.msgid
          AND aux.msg_uuid.uuid IN (
               SELECT uuid FROM aux.msg_uuid
               EXCEPT SELECT uuid FROM main.msg_uuid)
    } {
      message_insert $data $priority
    }
  }
  message_rebuild_catalog_when_idle
}

# Save the raw text of a single email message to the file descriptor
#
proc export_raw {out msgid} {
  db eval {
    SELECT data FROM msg JOIN blob USING(blobid) WHERE msgid=$msgid
  } break
  if {![info exists data]} return
  if {[regexp "^X-From: (\[^\n]*\n)" $data all header]} {
    set n [string length $all]
    puts -nonewline $out "From $header[string range $data $n end]"
    return
  }
  set source {unknown}
  set date [clock format [clock seconds]]
  regexp "Return-Path: <(\[^>\n\r\]+)>" $data all source
  regexp "Date: (\[^\n\r\]+)" $data all date
  puts -nonewline $out "From $source $date\n$data"
}

# Prompt the user for the name of a file, then append the raw text
# of the currently selected message into that file.
#
proc export {type} {
  set types {{{All Files} *}}
  set file [tk_getSaveFile -filetypes $types]
  if {$file==""} return
  set out [open $file a]
  fconfigure $out -translation auto
  if {$type=="selected"} {
    global selected
    export_raw $out $selected
  } elseif {$type=="visible"} {
    memdb eval {SELECT msgid FROM map} {
      export_raw $out $msgid
      update
    }
  } elseif {$type=="all"} {
    db eval {SELECT msgid FROM msg} {
      export_raw $out $msgid
      update
    }
  }
  close $out
}
