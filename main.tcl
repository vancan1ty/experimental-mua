################################### main.tcl ##################################
# A Mail User Agent for Tcl/Tk
#
# This module contains all of the code to initialize the user interface and
# start the system running.
#
package require Tk
package require sqlite3
package require mime
package require smtp
package require md4

# Open the email database.  Create a new one if necessary
#
set email_db_location [glob ~]/.email-db
if {$argc > 0} {;# then the user specified a location for the email db!
   set email_db_location [lindex $argv 0]
}
puts "argc: $argc, argv: $argv"

sqlite3 db $email_db_location 
db timeout 4000
catch {db transaction {db eval {
  -- General configuraton infomation is stored in the var() table.
  -- var.name1 and var.name2 are keys.  var.value is the data.
  --
  create table var(
    name1 text,
    name2 text,
    value any,
    primary key(name1, name2)
  );
  
  -- The body of email messages (which can be very large) are stored in 
  -- the BLOB table separate from the main MSG table.  This allows us to
  -- do scans of the MSG table without having to skip over the huge
  -- blobs of email content.  
  --
  create table blob(
    blobid integer primary key,
    data blob
  );
  
  -- One entry in this table for each email message.
  --
  create table msg(
    msgid integer primary key,
    blobid integer references content,
    priority integer,   -- 0==spam, 1==trash, ..., 4==high, 5==unread
    rcvd datetime,
    size integer,
    nattach integer,
    userid integer,
    subject text
  );
  create index msg_idx_rcvd on msg(rcvd, subject);
  create index msg_idx_subj on msg(subject);
  create index msg_idx_xfrom on msg(userid);

  -- Message IDs are used for merging databases.  They are stored
  -- separately from the MSG table for efficiency.
  --
  create table msg_uuid(
    msgid integer primary key,
    uuid text unique
  );

  -- Users that we have received email from or that we have sent email
  -- to or who have appeared on the To: or Cc: lists of emails we have
  -- received.
  --
  create table user(
    userid integer primary key,        -- Linkage to MSG.USERID
    addr text collate nocase unique,   -- userid@domain.name
    fulladdr text,                     -- "Human Name" <userid@domain.name>
    recent datetime,            -- time most recently seen
    nto integer default 0,      -- number of times in To: or Cc:
    nfrom integer default 0     -- number of times in From:
  );
  
  -- Search tags associate content words to the message.  The tags
  -- are all lower case.  Any string of three or more alphabetic
  -- characters in the message subject or body or To: or From:
  -- gets transformed into a tag.  The TAGXREF table maps every
  -- occurrence of a a tag to the email message where it occurred.
  -- This allows for very rapid searches by keyword.
  --
  create table tag(
    tagid integer primary key,
    word text
  );
  create index tag_idx1 on tag(word);
  create table tagxref(
    tagid integer,
    msgid integer,
    primary key(tagid,msgid)
  );
  create index tag_idx2 on tagxref(msgid);
}}}

# Create an in-memory database for holding transient data structures
#
sqlite3 memdb :memory:
memdb eval {
  -- For mapping line numbers in the catalog screen to message ids
  create table map(
    msgid integer primary key,  -- the message id
    lineno integer              -- the line number in the catalog text widget
  );
  create index mapidx2 on map(lineno);

  -- For mapping mime elements in the mime display into information
  -- needed to extract the mime.  This is used in the "MIME" display
  -- format.
  --
  create table mime(
    lineno integer,   -- Line number of "view" or "save" hyperlink
    partid text       -- MIME subcomponent index
  );
}

# Construct the main user interface screen
#
wm title . {Tcl/Tk Email Client}
wm iconname . {Tk-Email}
menu .mb -type menubar
if {$tcl_platform(platform)=="unix"} {
  pack .mb -side top -fill x
} else {
  . config -menu .mb
}
.mb add cascade -label File -underline 0 -menu .mb.file
.mb add cascade -label Edit -underline 0 -menu .mb.edit
.mb add cascade -label Debug -underline 0 -menu .mb.debug

menu .mb.file -tearoff 0
.mb.file add command -label {New Message...} -command {compose_begin}
.mb.file add command -label {Save As...} -command {export selected}
.mb.file add command -label {Export Visible...} -command {export visible}
.mb.file add command -label {Export All...} -command {export all}
.mb.file add command -label {Import From File...} -command import_from_file
.mb.file add command -label {Import From DB...} -command import_from_db
.mb.file add command -label {Configure...} -command config_popup
.mb.file add command -label {Fetch Mail...} -command fetch_begin
.mb.file add command -label {Empty Trash} \
     -command {message_delete_low_priority 1}
.mb.file add separator
.mb.file add command -label Exit -command exit

menu .mb.edit -tearoff 0
.mb.edit add command -label Cut -underline 2 -accel Ctrl-X \
   -state disabled
.mb.edit add command -label Copy -underline 0 -state disabled \
   -accel Ctrl-C
.mb.edit add command -label Paste -underline 0 -state disabled \
   -accel Ctrl-V
.mb.edit add command -label Delete -underline 0 -state disabled \
   -accel Del

menu .mb.debug -tearoff 0
.mb.debug add command -label {Tcl Command Shell...} -command ::console::start
.mb.debug add command -label {SQLite Console...} \
      -command {::sqlitecon::create .sqlcon {sqlite> } {SQLite Console} db}
.mb.debug add command -label {Tcl Proc Editor...} -command ::ped::create

frame .bb0
pack .bb0 -side top -fill x
proc ckbtn {w text var} {
  checkbutton $w -text $text -var $var -padx 0 -pady 0
  pack $w -side left -fill y
}
label .bb0.label -text {Show:}
pack .bb0.label -side left
ckbtn .bb0.unsent Unsent show(7)
ckbtn .bb0.sent Send show(6)
ckbtn .bb0.unread Unread show(5)
ckbtn .bb0.hi Hi show(4)
ckbtn .bb0.med Norm show(3)
ckbtn .bb0.lo Low show(2)
ckbtn .bb0.trash Trash show(1)
ckbtn .bb0.spam Spam show(0)
array set show {0 0 1 0 2 0 3 0 4 1 5 1 6 0 7 1}

frame .bb1
pack .bb1 -side top -fill x
proc setbtn {w args} {
  eval button $w $args -padx 0 -pady 0 -width 6 -highlightthickness 0
  pack $w -side left -fill y
}
label .bb1.label -text {Set Priority:}
pack .bb1.label -side left
setbtn .bb1.sent -text Sent -command {message_set_priority 6}
setbtn .bb1.unread -text Unread -command {message_set_priority 5}
setbtn .bb1.hi -text Hi -command {message_set_priority 4}
setbtn .bb1.med -text Norm -command {message_set_priority 3}
setbtn .bb1.lo -text Low -command {message_set_priority 2}
setbtn .bb1.trash -text Trash -command {message_set_priority 1}
setbtn .bb1.spam -text Spam -command {message_set_priority 0}

frame .bb2
pack .bb2 -side top -fill x -after .bb0
label .bb2.label -text {Order By:}
pack .bb2.label -side left
set orderby Date
::combobox::create .bb2.orderby -variable ::orderby \
    -width 8 -choices {Date Subject Sender Size}
pack .bb2.orderby -side left
set limit {30 Days}
label .bb2.label2 -text {Limit:}
pack .bb2.label2 -side left -padx {10 0}
::combobox::create .bb2.limit -variable ::limit \
    -width 10 -choices {{48 Hours} {7 Days} {30 Days} {90 Days} None}
pack .bb2.limit -side left

frame .bb3
pack .bb3 -side top -after .bb0 -fill x
label .bb3.label -text {Keywords:}
pack .bb3.label -side left
entry .bb3.e -bd 1 -relief sunken -bg white -fg black -textvariable keywords
pack .bb3.e -side left -fill x -expand 1
bind .bb3.e <Return> {.bb3.go invoke}
button .bb3.clr -text Clear -padx 0 -pady 0 -command {set ::keywords {}}
button .bb3.go -text Go -padx 0 -pady 0 \
  -command message_rebuild_catalog
pack .bb3.clr .bb3.go -side left

frame .bb  ;# The button bar
pack .bb -side top -fill x
proc bbbutton {w args} {
  eval button $w -padx 0 -pady 0 -highlightthickness 0 $args
  pack $w -side left -fill y
}
bbbutton .bb.fetch -text Fetch -command fetch_begin
bbbutton .bb.cat -text List -command {raise .tcat; raise .vsbcat}
bbbutton .bb.msg -text Msg -command {message_display}
bbbutton .bb.raw -text Raw -command {message_display_raw}
bbbutton .bb.mime -text Mime -command {message_display_mime}
bbbutton .bb.nx -text Next -command {message_next; message_display}
bbbutton .bb.prev -text Prev -command {message_previous; message_display}
bbbutton .bb.new -text New -command {compose_begin}
bbbutton .bb.reply1 -text Reply -command {compose_reply [compose_begin] 0}
bbbutton .bb.reply2 -text Reply-All -command {compose_reply [compose_begin] 1}
bbbutton .bb.fwd -text Forward -command {compose_forward [compose_begin]}
#bbbutton .bb.cut -text Cut -command {cut_text $toptext}
bbbutton .bb.copy -text Copy -command {copy_text $toptext}
#bbbutton .bb.paste -text Paste -command {paste_text $toptext}


text .t -yscrollcommand {.vsb set} -cursor top_left_arrow -bg white \
   -exportselection 0
.t tag config mimesave -foreground blue
.t tag config mimeview -foreground blue
.t tag bind mimesave <1> {mime_extract save %x %y}
.t tag bind mimeview <1> {mime_extract view %x %y}
pack .t -side left -fill both -expand 1
scrollbar .vsb -orient vertical -command {.t yview}
pack .vsb -side left -fill y
text .tcat -yscrollcommand {.vsbcat set} -cursor top_left_arrow -bd 0 \
   -highlightthickness 0 -bg white
place .tcat -in .t -x 0 -y 0 -relwidth 1.0 -relheight 1.0 
scrollbar .vsbcat -orient vertical -command {.tcat yview} \
   -highlightthickness 0
place .vsbcat -in .vsb -x -2 -y -2 -relwidth 1.0 -relheight 1.0 \
   -width 4 -height 4
.tcat tag config unread -foreground blue
.tcat tag config sel -background skyblue
bindtags .tcat {.tcat . all}
bind .tcat <1> {message_select %x %y; message_display}

# Make the message text box appear on top
#
proc show_msg {} {
  global toptext
  set toptext .t
  raise .t
  raise .vsb
}

# Make the catalog text box appear on top
#
proc show_catalog {} {
  global toptext
  set toptext .tcat
  raise .tcat
  raise .vsbcat
}

# Copy, Cut, or Paste the selection from text widget $t into the clipboard.
#
proc copy_text {t} {
  if {![catch {$t get sel.first sel.last} text]} {
     global SELECTION
     selection own -selection PRIMARY -command lose_selection .
     clipboard clear -displayof $t
     clipboard append -displayof $t $text
  }
}
proc cut_text {t} {
  copy_text $t
  catch {$t delete sel.first sel.last}
}
proc paste_text {t} {
  if {![catch {selection get} topaste] ||
      ![catch {selection get -displayof $t -selection CLIPBOARD} topaste]} {
    $t insert insert $topaste
  }
}
proc lose_selection {} {
  global SELECTION
  set SELECTION {}
}
proc retrieve_selection {offset max} {
  global SELECTION
  return [string range $SELECTION $offset [expr {$offset+$max}]]
}
selection handle -selection PRIMARY . retrieve_selection

# Automatically rebuild the catalog display when various search
# parameters change.
#
trace add variable orderby write message_rebuild_catalog_when_idle
trace add variable limit write message_rebuild_catalog_when_idle
trace add variable show write message_rebuild_catalog_when_idle

# The modules of this program can be source-ed in any order.  So when
# this module runs, we do not know if other modules are running yet or
# not.  We assume not.  So anything that depends on another module
# has to go inside the following _start procedure where it will be
# run by an idle callback after all files have been source-ed.
#
proc _start {} {
  rename _start {}
  message_rebuild_catalog_when_idle
}
after idle _start
