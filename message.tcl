############################# message.tcl ##################################
# This module contains code for processing and displaying email messages.
#

# Insert a message into the database.  Give it an initial priority of
# $priority.  The raw text of the message is in $msg
#
# If the message is already in the database (as determined by the uuid
# which is derived from the Message-ID: field of the header) then
# the insert fails silently.
#
proc message_insert {msg priority} {
  fetch_message "Saving message\n"
  #take away any ending whitespace!  that breaks the mime stuff if its there apparently...
  #set msg [string trim $msg]
  #-canonical text/plain
  #save the message to a file for further examination

  set fileId [open "mymessage.txt" "w"]
  puts -nonewline $fileId $msg
  close $fileId

  #read it back in because this magically fixes the problem
  set fp [open mymessage.txt r]
  set file_data [read $fp]
  set m [mime::initialize -string $file_data]

  #set m [mime::initialize  -string $msg]
  set uuid [message_uuid $m]
  fetch_message "UUID: '$uuid'\n"
  if {[db one {SELECT msgid FROM msg_uuid WHERE uuid=$uuid}]!=""} {
    mime::finalize $m
    return
  }
  set hdr(From) {}
  set hdr(Subject) {}
  array set hdr [mime::getheader $m]
  array set px [mime::getproperty $m]
  set vocab [message_vocabulary $m]
  mime::finalize $m
  puts "==========found hdr start=========="
  parray hdr
  puts "==========found hdr end==========="
  set from [lindex $hdr(From) 0]
  set subj [subject_fixup [lindex $hdr(Subject) 0]]
  if {![info exists hdr(X-From)] || [catch {
    set datestr [lrange [lindex $hdr(X-From) 0] 2 end]
    set date [clock scan $datestr]
  }]} {
    set date [clock seconds]
  } 
  if {![info exists px(parts)]} {
    set nattach 0
  } else {
    set nattach [expr {[llength $px(parts)]-1}]
  }
  set msgsize [string length $msg]
  set rcvd [db eval {SELECT julianday($date, 'unixepoch')}]
  db transaction {
    db eval {INSERT INTO blob(data) VALUES($msg)}
    set blobid [db last_insert_rowid]
    puts "found from '$from', rcvd '$rcvd'"
    set userid [user_insert $from $rcvd 1]
    puts "found userid is '$userid', subject is $subj"
    db eval {
      INSERT INTO msg(blobid, priority, rcvd, size, nattach, userid, subject)
        VALUES($blobid, $priority, $rcvd, $msgsize, $nattach, $userid, $subj);
    }
    set msgid [db last_insert_rowid]
    db eval {
      INSERT INTO msg_uuid(msgid,uuid) VALUES($msgid,$uuid)
    }
    foreach t $vocab {
      set tagid [db one {SELECT tagid FROM tag WHERE word=$t}]
      if {$tagid==""} {
        db eval {INSERT INTO tag(word) VALUES($t)}
        set tagid [db last_insert_rowid]
      }
      db eval {INSERT INTO tagxref VALUES($tagid,$msgid)}
    }
    foreach x {Cc Bcc} {
      if {[info exists hdr($x)]} {
        foreach line $hdr($x) {
          user_insert $line $rcvd 0
        }
      }
    }
  }
}


# Extract the Message-ID: from the header in the given mime.
# If there is no Message-ID attribute in the header, make one
# up using a hash of the complete header content.
#
proc message_uuid {m} {
  foreach hdr [mime::getheader $m -names] {
    if {[string compare -nocase $hdr message-id]==0} {
      return [mime::getheader $m $hdr]
    }
  }
  return [md4::md4 -hex [mime::getheader $m]]
}


# The string $addr contains zero or more email addresses.  Parse them
# up and insert them into the database.  Use $time to update the
# user.recent field.  Increment the user.nfrom field if $fromflag is
# true, otherwise increment the user.nto field.
#
# Return the userid of the last user inserted.
#
proc user_insert {addr time fromflag} {
  if {$fromflag} {
    set incrsql {UPDATE user SET nfrom=nfrom+1 WHERE userid=$x(userid)}
  } else {
    set incrsql {UPDATE user SET nto=nto+1 WHERE userid=$x(userid)}
  }
  set userid {}
  db transaction {
    foreach alist [mime::parseaddress $addr] {
      unset -nocomplain x
      array set x $alist
      if {[regsub {^"'(.*)'" <} $x(proper) {"\1" <} proper2]} {
        set x(proper) $proper2
      }
      set fulladdr $x(proper)
      db eval {SELECT userid, fulladdr AS oldfulladdr, recent
               FROM user WHERE addr=$x(address)} x break
      if {![info exists x(userid)]} {
        db eval {
          INSERT INTO user(addr,fulladdr,recent)
           VALUES($x(address),$fulladdr,$time)
        }
        set x(recent) $time
        set x(userid) [db last_insert_rowid]
      }
      db eval $incrsql
      if {$x(recent)<$time} {
        if {!$fromflag || $x(friendly)==$x(local)} {
          set fulladdr $x(oldfulladdr)
        }
        db eval {UPDATE user SET recent=$time, fulladdr=$fulladdr
                  WHERE userid=$x(userid)}
      }
      set userid $x(userid)
    }
  }
  return $userid
}

# Clean up a subject line.
#
# Remove all occurrences of Re:.  Remove excess whitespace.  Convert
# all newlines and tabs to a single space.  Do RFC 2047 decoding.
#
proc subject_fixup {orig} {
  regsub -all {\s+} [mime::field_decode $orig] { } x1
  regsub -all {R[eE](\[\d+\])?: +} $x1 {} x2
  return $x2
}

# The string $addr contains zero or more email addresses for a
# message that is being deleted.  Decrement the user.nfrom count
# if $fromflag is true or the user.nto count if $fromflag is false.
#
proc user_delete {addr fromflag} {
  if {$fromflag} {
    set incrsql {UPDATE user SET nfrom=nfrom-1 WHERE userid=$userid}
  } else {
    set incrsql {UPDATE user SET nto=nto-1 WHERE userid=$userid}
  }
  db transaction {
    foreach alist [mime::parseaddress $addr] {
      unset -nocomplain x
      array set x $alist
      set userid [db one {SELECT userid FROM user WHERE addr=$x(address)}]
      db eval $incrsql
    }
  }
}


# Extract and return the body of a message from a mime token.  The
# body is the first part we come to that has a content type of text/plain.
# This routine recursively decends the mime hierarchy of the message
# until it finds such a part.  If no part with content type text/plain
# is found, then an empty string is returned.
#
proc message_body {m} {
  array set px [mime::getproperty $m]
  set body {}
  if {![info exists px(parts)]} {
    set body [mime::getbody $m]
  } else {
    foreach part $px(parts) {
      set body [message_body $part]
      if {$body!=""} break
    }
  }
  return $body
}

# Extract a list of search keywords from the body of a message and
# selected parts of the header.  All keywords are at least 3 characters
# in length are are converted to all lower case.  There are no 
# duplicates on the list.
#
proc message_vocabulary {m} {
  set x {}
  array set hdr [mime::getheader $m]
  if {[info exists hdr(From)]} {
    set x $hdr(From)
  }
  if {[info exists hdr(Subject)]} {
    append x /[subject_fixup $hdr(Subject)]
  }
  append x /[message_body $m]
  regsub -all {[^a-zA-Z0-9_]} $x { } y
  unset x
  foreach token $y {
    if {[string length $token]<3} continue
    set token [string tolower $token]
    set seen($token) 1
  }
  return [array names seen]
}

# Remove a message from the database
#
proc message_delete {msgid} {
  db transaction {
    db eval {SELECT blobid FROM msg WHERE msgid=$msgid} break
    if {![info exists blobid]} return
    db eval {
      DELETE FROM msg_uuid WHERE msgid=$msgid;
      DELETE FROM msg WHERE msgid=$msgid;
      DELETE FROM blob WHERE blobid=$blobid;
      DELETE FROM tagxref WHERE msgid=$msgid;
    }
  }
}

# Remove all messages with a priority of N or less
#
proc message_delete_low_priority {n} {
  db transaction {
    db eval {
      CREATE TEMP TABLE todel AS SELECT msgid FROM msg WHERE priority<=$n;
      DELETE FROM blob WHERE blobid in
        (SELECT blobid FROM msg WHERE msgid IN todel);
      DELETE FROM msg_uuid WHERE msgid IN todel;
      DELETE FROM msg WHERE msgid IN todel;
      DELETE FROM tagxref WHERE msgid IN todel;
      DROP TABLE todel;
    }
  }
  global show
  for {set i 0} {$i<$n} {incr i} {
    if {![info exists show($i)]} break
    if {$show($i)} {
      message_rebuild_catalog_when_idle
      break
    }
  }
}

# Display the the message shows msgid is in the $::selected variable.
#
proc message_display {} {
  global selected
  set msgid $selected
  .t delete 1.0 end
  db eval {
    SELECT data, priority, fulladdr, size,
           subject, datetime(rcvd) AS received, nattach
      FROM msg JOIN blob USING(blobid) JOIN user ON msg.userid=user.userid
     WHERE msg.msgid=$msgid
  } break
  if {![info exists data]} return
  if {$priority==5} {
    message_set_priority 4
  }
  .t insert end "From: $fulladdr\n"
  .t insert end "Rcvd: $received  Id: $msgid\n"
  .t insert end "Subject: $subject\n"
  if {$nattach>0} {
    .t insert end "Attachments: $nattach   Total size: $size\n"
  }
  .t insert end \n
  #-canonical text/plain

  #eliminate CRLF linefeeds before display to the user!
  regsub -all {\r\n} $data "
" data

  set m [mime::initialize -string $data]
  .t config -wrap word
  set body [message_body $m]
  mime::finalize $m
  #CB don't strip out tabs -- I like tabs!
  #set body [string map [list \r { } \t {  }] $body]
  regsub -all "\[^!-~ \t\n\]" $body {_} body2
  .t insert end $body2
  show_msg
}


# Display the raw text of the message whose msgid is in
# the $::selected variable.
#
proc message_display_raw {} {
  global selected
  set msgid $selected
  .t delete 1.0 end
  db eval {
    SELECT data FROM msg JOIN blob USING(blobid) WHERE msgid=$msgid
  } break
  if {![info exists data]} return
  .t config -wrap char
  .t insert end $data
  show_msg
}

# Display a MIME composition of the message whose msgid is in 
# the $::selected variable.  Hyperlinks in this display allow
# the user to view or save to disk any subpart of the MIME.
#
proc message_display_mime {} {
  global selected
  set msgid $selected
  .t delete 1.0 end
  db eval {
    SELECT data, priority, fulladdr, subject, datetime(rcvd) AS received
      FROM msg JOIN blob USING(blobid) JOIN user ON msg.userid=user.userid
     WHERE msg.msgid=$msgid
  } break
  if {![info exists data]} return
  .t insert end "From: $fulladdr\n"
  .t insert end "Rcvd: $received  Id: $msgid\n"
  .t insert end "Subject: $subject\n\n"
  set m [mime::initialize -string $data]
  .t config -wrap char
  memdb eval {DELETE FROM mime}
  set n 0
  _show_mime_info {} $m
  mime::finalize $m
  show_msg
}
proc _show_mime_info {prefix m} {
  upvar 1 n n
  if {$prefix!=""} {append prefix .}
  array set px [mime::getproperty $m]
  if {[info exists px(params)]} {
    array set mx $px(params)
    unset px(params)
  }
  foreach property [array names px] {
    if {$property=="parts"} continue
    if {$px($property)==""} continue
    .t insert end ${prefix}$property=[list $px($property)]\n
  }
  foreach x [array names mx] {
    if {$mx($x)==""} continue
    .t insert end ${prefix}$x=[list $mx($x)]\n
  }
  if {![info exists px(parts)]} {
    scan [.t index {end -1 chars}] %d lineno
    memdb eval {INSERT INTO mime(lineno,partid) VALUES($lineno,$n)}
    incr n
    .t insert end ${prefix}content {} { save} mimesave " view\n" mimeview
  } else {
    set i 1
    foreach p $px(parts) {
      _show_mime_info ${prefix}m$i $p
      incr i
    }
  }
}

# This routine is called when the user clicks on the "save" or "view"
# links of a mime-view of a message.  Extract the content from the
# mime and either save it or view it.  The $destination argument 
# determines whether the result is saved to disk or displayed.  The
# $x and $y arguments are where the user clicked and are used to figure
# out which "save" or "view" hyperlink was clicked on.
#
proc mime_extract {destination x y} {
  set loc [.t index @$x,$y]
  set line [expr {int($loc)}]
  set n [memdb one {SELECT partid FROM mime WHERE lineno=$line}]
  global selected
  set msgid $selected
  db eval {SELECT data FROM msg JOIN blob USING( blobid ) WHERE msgid=$msgid} {
    set m [mime::initialize -string $data]
    set part [_find_subpart $m]
    if {![catch {array set px [mime::getproperty $part params]}]
         && [info exists px(name)]} {
      regsub -all {[^-a-zA-Z0-9_.]} $px(name) _ filename
    } else {
      set filename {}
    }
    set body [mime::getbody $part]
    mime::finalize $m
    break
  }
  if {![info exists body]} return
  if {$destination=="view"} {
    .t delete 1.0 end
    .t insert end $body
  } elseif {$destination=="save"} {
    if {$filename!=""} {
      set suffix [file extension $filename]
      set typename "[string toupper [string range $suffix 1 end]] Files"
      set types [list [list $typename $suffix] [list {All Files} *]]
    } else {
      set types {{{All Files} *}}
    }
    set fn [tk_getSaveFile -filetypes $types -initialfile $filename]
    if {$fn!=""} {
      if {![catch {
        set out [open $fn w]
        fconfigure $out -translation auto
        puts -nonewline $out $body
      } msg]} {
        catch {close $out}
      } else {
        tk_messageBox -icon error -type ok -message $msg
      }
    }
  }
}

# Locate the n-th subpart of the MIME object $m.  $n is a variable
# in the calling context.  This routine decrements the value $n for
# the number of parts it sees.  When $n reaches zero, it returns
# that subpart.  If $n never reaches zero, this routine returns an
# empty string.
#
proc _find_subpart {m} {
  upvar 1 n n
  array set px [mime::getproperty $m]
  if {![info exists px(parts)]} {
    if {$n>0} {
      incr n -1
      return {}
    }
    return $m
  } else {
    set i 1
    foreach p $px(parts) {
      set x [_find_subpart $p]
      if {$x!=""} {return $x}
      incr i
    }
  }
  return {}
}

# Rebuild the catalog display.
#
# The $::show() variable determines which priorities are shown.
#
# The priorities are:  7=unsent 6=sent 5=unread, 4=high, 3=normal, 2=low,
# 1=trash, 0=spam.
#
# The $::order by variable determines how the messages are sorted.
# The $::keywords variable determines any search keywords used to
# limit the display.
#
proc message_rebuild_catalog {} {
  global show orderby keywords selected rebuild_timer limit
  catch {after cancel $rebuild_timer; unset rebuild_timer}
  .tcat config -wrap none
  .tcat delete 1.0 end
  memdb eval {DELETE FROM map}
  set sql {
    SELECT msgid, priority, datetime(rcvd) AS rx, size, nattach,
           fulladdr, subject
      FROM msg JOIN user USING( userid )
  }
  set toshow {}
  for {set i 0} {$i<=7} {incr i} {
    if {$show($i)} {lappend toshow $i}
  }
  if {[llength $toshow]==0} {
    return
  }
  if {[llength $toshow]==1} {
    append sql "WHERE priority=[lindex $toshow 0] "
  } else {
    append sql "WHERE priority in ([join $toshow ,]) "
  }
  regsub -all {[^a-zA-Z0-9_]} [string tolower $keywords] { } srch
  if {[llength $srch]==0} {
    set taglist {}
    set ntag 0
  } else {
    set wordlist [join $srch ',']
    set taglist [db eval "SELECT tagid FROM tag WHERE word IN ('$wordlist')"]
    set ntag [llength $taglist]
    if {$ntag<[llength $srch]} {return}
  }
  if {$limit!="None"} {
    append sql "AND rcvd>=(SELECT julianday('now','-$limit')) "
  }
  if {$ntag==0} {
    # append sql "AND rcvd>=(SELECT julianday('now','-1 month')) "
  } elseif {$ntag==1} {
    set tag [lindex $taglist 0]
    append sql "AND EXISTS(SELECT * FROM tagxref \
                            WHERE tagid=$tag AND msgid=msg.msgid) "
  } else {
    set tags [join $taglist ,]
    append sql "AND $ntag==(SELECT count(*) FROM tagxref \
                             WHERE tagid IN ($tags) AND msgid=msg.msgid) "
  }
  switch $orderby {
    Date {
      append sql "ORDER BY rcvd"
    }
    Subject {
      append sql "ORDER BY subject, rcvd"
    }
    Sender {
      append sql "ORDER BY addr, rcvd"
    }
    Size {
      append sql "ORDER BY size, rcvd"
    }
  }
  set lineno 0
  db eval $sql {
    incr lineno
    memdb eval {INSERT INTO map(lineno,msgid) VALUES($lineno,$msgid)}
    if {$priority==5} {set tag unread} {set tag {}}
    if {$orderby=="Size"} {
      set line "Size: $size Subj: $subject From: $fulladdr Rcvd: $rx"
    } else {
      set line "Subj: $subject From: $fulladdr Rcvd: $rx"
    }
    regsub -all \n $line { } line2
    .tcat insert end $line2\n $tag
  }
  if {![info exists selected] || $selected==""} {
    set selected [memdb one {SELECT msgid FROM map ORDER BY lineno DESC}]
  }
  message_make_current $selected
  show_catalog
}
proc message_rebuild_catalog_when_idle {args} {
  global rebuild_timer
  if {![info exists rebuild_timer]} {
    set rebuild_timer [after idle message_rebuild_catalog]
  }
}

# Remove the indicated message (the message id $msgid) from the catalog.
# To avoid having to rebuild the whole catalog display, we remove the
# particular row and adjust the MAP table in the memdb database
# accordingly.
#
proc message_remove {msgid} {
  global selected
  if {$msgid!=""} {
    if {$msgid==$selected} {message_next}
    set line [memdb one {SELECT lineno FROM map WHERE msgid=$msgid}]
    .tcat delete $line.0 [list $line.0 lineend +1 char]
    memdb eval {
      DELETE FROM map WHERE msgid=$msgid;
      UPDATE map SET lineno=lineno-1 WHERE lineno>=$line;
    }
  }
}

# Change the priority of the currently selected message.  If the
# new priority is one that is not displayed (according to the
# $::show() array variable) then remove the message from the 
# catalog and advance the display to the next message automatically.
#
proc message_set_priority {newpriority} {
  global selected show
  set msgid $selected
  if {$msgid!=""} {
    set oldpri [db one {SELECT priority FROM msg WHERE msgid=$msgid}]
    if {$oldpri==$newpriority} return
    if {$oldpri==5} {
      set line [memdb one {SELECT lineno FROM map WHERE msgid=$msgid}]
      .tcat tag remove unread $line.0 [list $line.0 lineend +1 char]
    }
    if {$oldpri<5 && !$show($newpriority)} {
      message_next
      message_display
    }
    db eval {UPDATE msg SET priority=$newpriority WHERE msgid=$msgid}
    if {!$show($newpriority)} {
       message_remove $msgid
    } elseif {$newpriority==5} {
      set line [memdb one {SELECT lineno FROM map WHERE msgid=$msgid}]
      .tcat tag add unread $line.0 [list $line.0 lineend +1 char]
    }
  }
}

# Cause the message whose catalog line is at $x, $y to be the current
# message.
#
proc message_select {x y} {
  set loc [.tcat index @$x,$y]
  set line [expr {int($loc)}]
  set msgid [memdb eval {SELECT msgid FROM map WHERE lineno=$line}]
  if {$msgid!=""} {
    message_make_current $msgid
  }
}

# Make the indicated message the current message in the catalog.
# Make sure it is visible in the catalog and highlight it.
#
proc message_make_current {msgid} {
  global selected
  set line [memdb one {SELECT lineno FROM map WHERE msgid=$msgid}]
  .tcat tag remove sel 1.0 end
  if {$line!=""} {
    .tcat tag add sel $line.0 [list $line.0 lineend +1 char]
    .tcat see $line.0
  }
  set selected $msgid
}

# Advance to the next message down in the catalog.
#
proc message_next {} {
  global selected
  set msgid [memdb one {SELECT msgid FROM map WHERE lineno=
                        (SELECT lineno FROM map WHERE msgid=$selected)+1}]
  if {$msgid==""} {
    set msgid [memdb one {SELECT msgid FROM map ORDER bY lineno DESC limit 1}]
  }
  message_make_current $msgid
}

# Move the selection back up to the previous message
#
proc message_previous {} {
  global selected
  set msgid [memdb one {SELECT msgid FROM map WHERE lineno=
                       (SELECT lineno FROM map WHERE msgid=$selected)-1}]
  message_make_current $msgid
}
