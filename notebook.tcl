############################# notebook.tcl #################################
# A new notebook widget for Tk that looks like the notebook in Win2K.
#

# Create the ::notebook namespace to contain all the code, and a state
# variable within that namespace.
#
namespace eval ::notebook {
  variable v
}


# proc:  ::notebook::create WIDGET ARGS...
# title: Create a new notebook widget
#
# Create a new notebook widget named WIDGET.  The widget is actually a
# canvas with bindings for changing pages when the user clicks on notebook
# tables.  Options for the widget are described on the ::notebook::config
# routine.
#
proc ::notebook::create {w args} {
  upvar #0 ::notebook::v$w v
  set v(width) 400
  set v(height) 300
  set v(pages) {}
  set v(top) 0
  set v(padx) 5
  set v(pady) 5
  set v(on) black
  set v(fg,off) grey50
  set v(active) 0
  canvas $w -bd 0 -highlightthickness 0 -takefocus 0
  frame $w.focus -takefocus 1
  place $w.focus -x -10 -y -10
  set v(bg) [$w cget -bg]
  bind $w.focus <FocusIn> [list ::notebook::redraw $w]
  bind $w.focus <FocusOut> [list ::notebook::redraw $w]
  bind $w.focus <Right> [list ::notebook::_traverse $w 1]
  bind $w.focus <Left> [list ::notebook::_traverse $w -1]
  bind $w <Configure> [list ::notebook::redraw $w]
  bind $w <Destroy> [subst {catch {unset ::notebook::v$w}}]
  $w bind tab <1> [list ::notebook::_click $w]
  eval ::notebook::config $w $args
}

# proc:   ::notebook::config WIDGET OPTION VALUE ...
# title:  Reconfigure a notebook widget
#
# Change the configuration of the notebook widget WIDGET.  The
# ::notebook::redraw is called automatically after everything is
# reconfigured.  Configuration options are as follows:
#
#   -width PIXELS
#
#         Minimum width of the notebook in pixels.  The actual width
#         may be larger if necessary to contain one or more of the
#         notebook pages.
#
#   -height PIXELS
#
#         Minimum height of the notebook in pixels.  The actual height
#         may be larger if necessary to contain the notebook pages
#
#   -pages NAMELIST
#
#         A list of names of the notebook tab labels.  For each tab,
#         a new frame named WIDGET.pNNN is automatically created where
#         WIDGET is the name of the notebook widget itself and NNN is
#         the sequence number of the tab.  The first tab is number 0.
#
#   -padx PIXELS
#
#         Amount of padding on the left and right of the notebook.  Normally,
#         the same amount of padding is put on both sides.  But if PIXELS is a
#         list of two numbers, then the first number is the padding on the left
#         and the second number is the padding on the right.
#
#   -pady PIXELS
#
#         Amount of padding on the top and bottom of the notebook.  Normally,
#         the same amount of padding is put on both sides.  But if PIXELS is a
#         list of two numbers, then the first number is the padding on the top
#         and the second number is the padding on the bottom.
#
#   -bg COLOR
#
#         The background color for the notebook
#
proc ::notebook::config {w args} {
  upvar #0 ::notebook::v$w v
  foreach {tag value} $args {
    switch -- $tag {
      -width {
        set v(width) $value
      }
      -height {
        set v(height) $value
      }
      -pages {
        set oldN [llength $v(pages)]
        set v(pages) $value
        set N [llength $v(pages)]
        for {set i $oldN} {$i<$N} {incr i} {
          frame $w.p$i
        }
        for {set i $N} {$i<$oldN} {incr i} {
          destroy $w.p$i
        }
      }
      -pady {
        if {[llength $value]>1} {
          set v(padtop) [lindex $value 0]
          set v(padbtm) [lindex $value 1]
        } else {
          set v(padtop) $value
          set v(padbtm) $value
        }
      }
      -padx {
        if {[llength $value]>1} {
          set v(padlft) [lindex $value 0]
          set v(padrgh) [lindex $value 1]
        } else {
          set v(padlft) $value
          set v(padrgh) $value
        }
      }
      -bg {
        set v(bg) $value
      }
      -fg {
        set v(fg,on) $value
      }
      -disabledforeground {
        set v(fg,off) $value
      }
    }
  }
  redraw $w
}

# proc:  ::notebook::redraw WIDGET
# title: Redraw the notebook widget
#
# This routine redraws a notebook widget after a reconfiguration or
# when a new tab is made active.  This routine is called automatically
# by ::notebook::config and ::notebook::raise.  But if some of the
# widgets on the inside of a page change their requested sizes, you
# may need to call this routine manually in order to get the notebook
# widget to automatically resize.
#
# Actually, this routine does not do any drawing directly.  It just
# makes arrangements to do the drawing in an idle callback.
#
proc ::notebook::redraw w {
  if {![winfo exists $w]} return
  upvar #0 ::notebook::v$w v
  if {[info exists v(pending)]} return
  set v(pending) [after idle [list ::notebook::_redraw $w]]
}

# proc:  ::notebook::raise WIDGET PAGENO
# title: Bring a particular page of the notebook to the top
#
# Cause page number PAGENO of the notebook widget WIDGET to come to the
# top and be displayed.  This is the same as clicking on the corresponding
# notebook tab.
#
proc ::notebook::raise {w pgno} {
  if {![winfo exists $w]} return
  upvar #0 ::notebook::v$w v
  if {$v(active)!=$pgno} {
    set v(active) $pgno
    redraw $w
  }
}

##############################################################################
# The public interface to this module consists of the routines above this
# line.  Everything below this line is private and should not be referenced,
# read, or invoked by client code.
##############################################################################

# This routine is invoked as an idle callback to redraw the notebook.
# This routine is for internal use only.  Do not call it directly.
# See ::notebook::redraw for details.
#
proc ::notebook::_redraw w {
  if {![winfo exists $w]} return
  upvar #0 ::notebook::v$w v
  catch {unset v(pending)}

  # Find maximum requested width and height for all pages
  #
  set nPage [llength $v(pages)]
  set mxrw 10
  set mxrh 10
  for {set i 0} {$i<$nPage} {incr i} {
    set rw [winfo reqwidth $w.p$i]
    set rh [winfo reqheight $w.p$i]
    if {$rw>$mxrw} {set mxrw $rw}
    if {$rh>$mxrh} {set mxrh $rh}
    if {$i!=$v(active)} {
      place forget $w.p$i
    }
  }

  # Generate text for all notebook tabs
  #
  $w delete all
  set i -1
  set mxtabh 0
  set sumtabw 0
  foreach tab $v(pages) {
    incr i
    set tabid($i) \
      [$w create text 0 0 -text $tab -anchor w -tags [list tab i$i]]
    foreach {x1 y1 x2 y2} [$w bbox $tabid($i)] break
    set tabw($i) [expr {16+$x2-$x1}]
    set tabh($i) [expr {$y2-$y1}]
    if {$tabh($i)>$mxtabh} {set mxtabh $tabh($i)}
    set sumtabw [expr {$sumtabw+$tabw($i)+3}]
  }
  if {$sumtabw+4>$mxrw} {
    set mxrw [expr {$sumtabw+4}]
  }

  # Compute the geometry
  #
  set y1 $v(padtop)                        ;# Top of selected tab
  set y2 [expr {$y1+1}]
  set y3 [expr {$y2+1}]                    ;# Top of unselected tab
  set y3b [expr {$y3+1}]
  set y3c [expr {$y3b+1}]
  set y6 [expr {$y3+$mxtabh+4}]            ;# Top of frame
  set y5 [expr {int(0.5*($y3+$y6))+2}]     ;# Center of selected text
  set y4 [expr {$y5-2}]                    ;# Center of unselected text
  set y7 [expr {$y6+1}]                    ;# Bottom of frame
  set y8 [expr {$y7+$mxrh}]
  if {$y8+$v(padbtm)+1<$v(height)} {
    set y8 [expr {$v(height)-$v(padbtm)-1}]
  }
  set y9 [expr {$y8+1}]
  set x1 $v(padlft)                        ;# Left edge of frame
  set x2 [expr {$x1+1}]
  set x3 [expr {$x2+$mxrw}]                ;# Right edge of frame
  if {$x3+1+$v(padrgh)<$v(width)} {
    set x3 [expr {$v(width) - $v(padrgh) - 1}]
  }
  set x4 [expr {$x3+1}]

  # Resize the canvas widget
  #
  $w config -width [expr {$x4+$v(padrgh)}] -height [expr {$y9+$v(padbtm)}]

  # Draw the 3D border around the frame
  #
  $w create line $x1 $y9 $x1 $y6 $x4 $y6 -fill white -width 1 -join miter
  $w create line $x2 $y8 $x4 $y8 -fill #828282
  $w create line $x3 $y8 $x3 $y7 -fill #828282
  $w create line $x1 $y9 $x4 $y9 -fill black
  $w create line $x4 [expr {$y9+1}] $x4 $y6 -fill black
  if {$v(active)>=0} {
      place $w.p$v(active) -x $x2 -y $y7 -width [expr {$x3-$x2}] \
        -height [expr {$y8-$y7}]
  }

  # Draw the tabs
  #
  set x $x1
  set i -1
  foreach tab $v(pages) {
    incr i
    if {$i==$v(active)} {
      $w coords $tabid($i) [expr {$x+11}] $y4
      if {[::focus]=="$w.focus"} {
        foreach {l t r b} [$w bbox $tabid($i)] break
        incr l -7
        incr r 8
        $w create line $l $t $r $t $r $b $l $b $l $t -stipple gray50
      }
      set lx0 $x
      set lx1 [expr {$lx0+1}]
      set lx2 [expr {$lx1+1}]
      set lx3 [expr {$lx1+$tabw($i)+2}]
      set lx4 [expr {$lx3+1}]
      set lx5 [expr {$lx4+1}]
      set ly6 [expr {$y6+1}]
      $w create line $lx0 $y6 $lx0 $y3 $lx2 $y1 $lx4 $y1 -fill white
      $w create line $lx4 $y2 $lx5 $y3 $lx5 $ly6 -fill black
      $w create line $lx4 $y3 $lx4 $ly6 -fill #828282
      $w create line $lx1 $y6 $lx4 $y6 -fill $v(bg)
    } else {
      $w coords $tabid($i) [expr {$x+11}] $y5
      set lx0 [expr {$x+2}]
      set lx1 [expr {$lx0+1}]
      set lx2 [expr {$lx1+1}]
      set lx3 [expr {$lx1+$tabw($i)}]
      set lx4 [expr {$lx3+1}]
      set lx5 [expr {$lx4+1}]
      if {$i!=$v(active)+1} {
        $w create line $lx0 $y6 $lx0 $y3c $lx2 $y3 $lx4 $y3 -fill white
      } else {
        $w create line $lx0 $y3 $lx4 $y3 -fill white
      }
      if {$i!=$v(active)-1} {
        $w create line $lx4 $y3b $lx5 $y3c $lx5 $y6 -fill black
        $w create line $lx4 $y3c $lx4 $y6 -fill #828282
      }
    }
    incr x [expr {$tabw($i)+4}]
  }

}

# This command runs when the user clicks on a notebook tab.  The notebook
# is redrawn to show the clicked-on tab as selected and the corresponding
# frame is raised.
#
proc ::notebook::_click w {
  upvar #0 ::notebook::v$w v
  foreach t [$w gettags current] {
    if {[regexp {^i([0-9]+)$} $t all num]} {
      if {$v(active)!=$num} {
        ::focus $w.focus
        set v(active) $num
        ::notebook::redraw $w
      }
      break
    }
  }
}

# This routine implement keyboard traversal of the notebook tabs.  It
# gets called when the active notebook table has focus and the user
# presses the left or right arrow keys.  Change which tab is selected
# and redraw the notebook widget.
#
proc ::notebook::_traverse {w d} {
  upvar #0 ::notebook::v$w v
  set nactive $v(active)
  incr nactive $d
  if {$nactive>=0 && $nactive<[llength $v(pages)]} {
    set v(active) $nactive
    redraw $w
  }
}
